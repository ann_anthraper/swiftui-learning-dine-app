//
//  AppView.swift
//  DineApp
//
//  Created by Ann Mary on 03/01/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI

struct AppView: View {
    
    var body: some View {
        TabView {
            ContentView()
                .tabItem {
                    Image(systemName: "list.dash")
                    Text("Menu")
            }
            OrderView()
                .tabItem {
                    Image(systemName: "square.and.pencil")
                    Text("Order")
            }
            FavoriteView()
                .tabItem {
                    Image(systemName: "heart")
                    Text("Favorites")
            }
        }
    }
}

struct TabView_Previews: PreviewProvider {
    static let order = Order()
    static let favorite = Favorite()
    
    static var previews: some View {
        AppView()
            .environmentObject(order)
            .environmentObject(favorite)
            
    }
}
