//
//  FavoriteView.swift
//  DineApp
//
//  Created by Ann Mary on 04/01/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI

struct FavoriteView: View {
    
    @EnvironmentObject var favorite: Favorite
    
    var body: some View {
        NavigationView {
            List {
                Section {
                    ForEach(favorite.items) { item in
                        HStack {
                            Image(item.thumbnailImage)
                            .clipShape(Circle())
                            .overlay(Circle().stroke(Color.gray, lineWidth: 2))
                            Text(item.name)
                        }
                    }.onDelete(perform: deleteItems)
                }
            }
            .navigationBarTitle("Favorites")
            .listStyle(GroupedListStyle())
            .navigationBarItems(trailing: EditButton())
        }
    }
    
    func deleteItems(at offsets:IndexSet){
        favorite.items.remove(atOffsets: offsets)
    }
    
}

struct FavoriteView_Previews: PreviewProvider {
    
    static let favorite = Favorite()
    
    static var previews: some View {
        FavoriteView().environmentObject(favorite)
    }
}
