//
//  ContentView.swift
//  DineApp
//
//  Created by Ann Mary on 03/01/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    let menu = Bundle.main.decode([MenuSection].self, from: "menu.json")
    
    var body: some View {
        NavigationView {
            List {
                ForEach(menu) { section in
                    Section(header: Text(section.name)
                        .font(.custom("Helvetica Neue", size: 30))
                        .foregroundColor(.secondary)) {
                        ForEach(section.items) {
                            item in
                            ItemRow(item:item)
                        }
                    }
                }
            }
            .navigationBarTitle("Menu")
            .listStyle(GroupedListStyle())
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().previewDevice("iPhone 11 Pro")
    }
}
