//
//  ItemDetail.swift
//  DineApp
//
//  Created by Ann Mary on 03/01/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI

struct ItemDetail: View {
    
    @EnvironmentObject var order: Order
    @EnvironmentObject var favorite: Favorite
    
    var item: MenuItem
    
    @State private var isFavorite = false
        
    var body: some View {
        VStack {
            ZStack(alignment: .bottomTrailing) {
                Image(item.mainImage)
                Text("Photo : \(item.photoCredit)")
                    .padding(4)
                    .background(Color.black)
                    .font(.caption)
                    .foregroundColor(.white)
                    .offset(x: -25, y: -5)
            }
            Text(item.description)
                .padding()
            
            Button("ORDER THIS") {
                self.order.add(item: self.item)
            }.padding(10)
                .background(Color.white)
                .overlay(RoundedRectangle(cornerRadius: 20)
                    .stroke(Color.gray.opacity(0.4), lineWidth: 2))
                .font(.custom("Marker Felt", size: 20))
            
            Spacer()
            
        }
        .navigationBarTitle(Text(item.name), displayMode: .inline)
        .navigationBarItems(trailing:
            Button(action: {
                self.isFavorite.toggle()
                if self.isFavorite {
                    self.favorite.add(item: self.item)
                }else {
                    self.favorite.remove(item: self.item)
                }
            }) {
                if isFavorite {
                    Image(systemName: "heart.fill").foregroundColor(.red)
                }else {
                    Image(systemName: "heart").foregroundColor(.red)
                }
            })
    }
  
}

struct ItemDetail_Previews: PreviewProvider {
    
    static let order = Order()
    static let favorite = Favorite()
    
    static var previews: some View {
        NavigationView {
            ItemDetail(item: MenuItem.example)
                .environmentObject(order)
                .environmentObject(favorite)
        }
    }
}
