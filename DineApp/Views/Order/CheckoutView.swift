//
//  CheckoutView.swift
//  DineApp
//
//  Created by Ann Mary on 03/01/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI

struct CheckoutView: View {
    @EnvironmentObject var order : Order
    
    static let paymentTypes = ["Cash", "Credit Card", "Loyalty Points"]
    @State private var paymentIndex = 0
    @State private var addLoyalty = false
    @State private var loyaltyNumber = ""
    
    static let tipAmounts = [10, 15, 20, 25, 0]
    @State private var tipIndex = 1
    
    static let deliveryTime = ["Now", "Tonight", "Tomrrow"]
    @State private var deliveryIndex = 2
    
    @State private var showAlert = false
    
    var totalPrice: Double {
        let total = Double(order.total)
        let tipvalue = total/100 * Double(Self.tipAmounts[tipIndex])
        return total + tipvalue
    }
    
    var body: some View {
        Form {
            Section {
                Picker("How do you want to Pay?", selection: $paymentIndex) {
                    ForEach(0 ..< Self.paymentTypes.count) {
                        Text(Self.paymentTypes[$0])
                    }
                }
                
                Toggle(isOn: $addLoyalty.animation()) {
                    Text("Add your Loyalty Card")
                }
                
                if addLoyalty {
                    TextField("Enter Loyalty ID", text: $loyaltyNumber)
                }
            }
            
            Section(header: Text("Add a tip ?")) {
                Picker("Percentage", selection: $tipIndex) {
                    ForEach(0 ..< Self.tipAmounts.count) {
                        Text("\(Self.tipAmounts[$0])%")
                    }
                }.pickerStyle(SegmentedPickerStyle())
            }
            
            Section(header: Text("Pickup Time :")) {
                Picker("Time", selection: $deliveryIndex) {
                    ForEach(0 ..< Self.deliveryTime.count){
                        Text(Self.deliveryTime[$0])
                    }
                }.pickerStyle(SegmentedPickerStyle())
            }
            
            
            Section(header: Text("Total $\(totalPrice, specifier: "%.2f")").font(.title)) {
                Button("Confirm Order") {
                    self.showAlert.toggle()
                }
            }
        }
        .navigationBarTitle(Text("Payment"),displayMode: .inline)
        .alert(isPresented: $showAlert) {
            Alert(title: Text("Order confirmed"), message: Text("Your total was $\(totalPrice, specifier: "%.2f") \n Thank You!"), dismissButton: .default(Text("OK")))

            
        }
    }
}

struct CheckoutView_Previews: PreviewProvider {
    static let order = Order()
    static var previews: some View {
        CheckoutView().environmentObject(order)
    }
}
