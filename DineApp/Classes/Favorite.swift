//
//  Favorite.swift
//  DineApp
//
//  Created by Ann Mary on 04/01/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI

class Favorite: ObservableObject {
    
    @Published var items = [MenuItem]()

    func add(item: MenuItem) {
        items.append(item)
    }

    func remove(item: MenuItem) {
        if let index = items.firstIndex(of: item) {
            items.remove(at: index)
        }
    }
}
